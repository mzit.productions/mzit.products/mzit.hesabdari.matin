﻿using System;
using System.Windows.Forms;

namespace Hesabdari
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                using (frmEnter _frmEnter = new frmEnter())
                {
                    if (_frmEnter.ShowDialog() != DialogResult.OK) return;
                }

                Application.Run(new frmMain());
            }
            catch (Exception exc)
            {

            }
        }
    }
}
