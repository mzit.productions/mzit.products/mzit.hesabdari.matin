﻿using System;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmChangepass : Form
    {
        public frmChangepass()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUsername.Text != "" && lblOldpass.Text != "" && txtNewpass.Text != "")
                {
                    matinDataSet.userDataTable ouserDataTable
                        = new matinDataSet.userDataTable();

                    matinDataSetTableAdapters.userTableAdapter ouserTableAdapter =
                        new matinDataSetTableAdapters.userTableAdapter();

                    if (ouserTableAdapter.CountUserByPassUser(txtUsername.Text, txtOldpass.Text) != 1)
                    {
                        MessageBox.Show("نام کاربری یا رمز ورود قبلی اشتباه است.");
                    }
                    else
                    {
                        ouserTableAdapter.UpdateUserByPassUser(txtUsername.Text, txtNewpass.Text, true, txtUsername.Text, txtOldpass.Text);

                        MessageBox.Show("رمز ورود کاربر با موفقیت تغییر یافت.");
                    }
                }
                else
                {
                    MessageBox.Show("فیلدهای مورد نظر را پر نمایید!");
                }
            }
            catch { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
