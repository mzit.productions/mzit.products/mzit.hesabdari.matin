﻿using System;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmChoiceCustomer : Form
    {
        public frmChoiceCustomer()
        {
            InitializeComponent();
        }

        private void btnChoice_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCustomers.SelectedRows[0].Cells[0].Value.ToString() != null && dgvCustomers.SelectedRows[0].Cells[0].Value.ToString() != "")
                {
                    Properties.Settings.Default.ChoiceCustomerID = string.Empty;
                    Properties.Settings.Default.ChoiceCustomerID = dgvCustomers.SelectedRows[0].Cells[0].Value.ToString();

                    Close();
                }
                else
                {
                    MessageBox.Show("کالای مورد نظر را انتخاب کنید.",
                                    "خطا",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error,
                                    MessageBoxDefaultButton.Button1);
                }
            }
            catch { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool RefreshCustomers()
        {
            try
            {
                matinDataSet.customerDataTable ocustomerDataTable
                        = new matinDataSet.customerDataTable();

                matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                    new matinDataSetTableAdapters.customerTableAdapter();

                ocustomerTableAdapter.FillCustomerByCustomers(ocustomerDataTable);

                if (ocustomerDataTable.Count != 0)
                {
                    dgvCustomers.DataSource = ocustomerDataTable;

                    int index = -1;
                    
                    dgvCustomers.Columns[++index].HeaderText = "شناسه مشتری";
                    dgvCustomers.Columns[++index].HeaderText = "کد ملی";
                    dgvCustomers.Columns[++index].HeaderText = "نام و نام خانوادگی";
                    dgvCustomers.Columns[++index].HeaderText = "تلفن همراه";
                    dgvCustomers.Columns[++index].HeaderText = "تلفن";
                    dgvCustomers.Columns[++index].HeaderText = "آدرس";
                }
            }
            catch { }

            return true;
        }

        private void frmChoiceCustomer_Load(object sender, EventArgs e)
        {
            try
            {
                RefreshCustomers();
            }
            catch { }
        }
    }
}
