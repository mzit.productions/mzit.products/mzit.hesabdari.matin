﻿using System;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmChoiceProduct : Form
    {
        public frmChoiceProduct()
        {
            InitializeComponent();
        }

        private void btnChoice_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvProducts.SelectedRows[0].Cells[0].Value.ToString() != null && dgvProducts.SelectedRows[0].Cells[0].Value.ToString() != "")
                {
                    Properties.Settings.Default.ChoiceProductID = string.Empty;
                    Properties.Settings.Default.ChoiceProductID = dgvProducts.SelectedRows[0].Cells[0].Value.ToString();

                    Close();
                }
                else
                {
                    MessageBox.Show("کالای مورد نظر را انتخاب کنید.",
                                    "خطا",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error,
                                    MessageBoxDefaultButton.Button1);
                }
            }
            catch { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool RefreshProducts()
        {
            try
            {
                matinDataSet.productDataTable oproductDataTable
                        = new matinDataSet.productDataTable();

                matinDataSetTableAdapters.productTableAdapter oproductTableAdapter =
                    new matinDataSetTableAdapters.productTableAdapter();

                oproductTableAdapter.FillProductByProducts(oproductDataTable);

                if (oproductDataTable.Count != 0)
                {
                    dgvProducts.DataSource = oproductDataTable;

                    int index = -1;

                    dgvProducts.Columns[++index].HeaderText = "شناسه کالا";
                    dgvProducts.Columns[++index].HeaderText = "کد کالا";
                    dgvProducts.Columns[++index].HeaderText = "نام کالا";
                    dgvProducts.Columns[++index].HeaderText = "قیمت کالا";
                }
            }
            catch { }

            return true;
        }

        private void frmChoiceProduct_Load(object sender, EventArgs e)
        {
            try
            {
                RefreshProducts();
            }
            catch { }
        }
    }
}
