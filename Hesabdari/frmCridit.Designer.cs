﻿namespace Hesabdari
{
    partial class frmCridit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.lblCodemeli = new System.Windows.Forms.Label();
            this.txtCodemeli = new System.Windows.Forms.TextBox();
            this.lblCridit = new System.Windows.Forms.Label();
            this.txtCridit = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAcountBalance = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(372, 204);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 28);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "ذخیره";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblCodemeli
            // 
            this.lblCodemeli.AutoSize = true;
            this.lblCodemeli.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodemeli.Location = new System.Drawing.Point(25, 44);
            this.lblCodemeli.Name = "lblCodemeli";
            this.lblCodemeli.Size = new System.Drawing.Size(98, 14);
            this.lblCodemeli.TabIndex = 1;
            this.lblCodemeli.Text = "کد ملی مشتری";
            // 
            // txtCodemeli
            // 
            this.txtCodemeli.Location = new System.Drawing.Point(129, 42);
            this.txtCodemeli.Name = "txtCodemeli";
            this.txtCodemeli.Size = new System.Drawing.Size(150, 20);
            this.txtCodemeli.TabIndex = 2;
            // 
            // lblCridit
            // 
            this.lblCridit.AutoSize = true;
            this.lblCridit.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCridit.Location = new System.Drawing.Point(16, 211);
            this.lblCridit.Name = "lblCridit";
            this.lblCridit.Size = new System.Drawing.Size(107, 14);
            this.lblCridit.TabIndex = 3;
            this.lblCridit.Text = "مبلغ شارژ حساب";
            // 
            // txtCridit
            // 
            this.txtCridit.Location = new System.Drawing.Point(129, 209);
            this.txtCridit.Name = "txtCridit";
            this.txtCridit.Size = new System.Drawing.Size(150, 20);
            this.txtCridit.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(285, 204);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 28);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "لغو";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(285, 37);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(81, 28);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "جست و جو";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.txtName.Location = new System.Drawing.Point(129, 98);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(150, 20);
            this.txtName.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 14);
            this.label1.TabIndex = 7;
            this.label1.Text = "نام و نام خانوادگی";
            // 
            // txtAcountBalance
            // 
            this.txtAcountBalance.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.txtAcountBalance.Location = new System.Drawing.Point(129, 124);
            this.txtAcountBalance.Name = "txtAcountBalance";
            this.txtAcountBalance.ReadOnly = true;
            this.txtAcountBalance.Size = new System.Drawing.Size(150, 20);
            this.txtAcountBalance.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 14);
            this.label2.TabIndex = 9;
            this.label2.Text = "موجودی حساب";
            // 
            // frmCridit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.ClientSize = new System.Drawing.Size(465, 252);
            this.Controls.Add(this.txtAcountBalance);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtCridit);
            this.Controls.Add(this.lblCridit);
            this.Controls.Add(this.txtCodemeli);
            this.Controls.Add(this.lblCodemeli);
            this.Controls.Add(this.btnSave);
            this.MaximizeBox = false;
            this.Name = "frmCridit";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "شارژ حساب";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblCodemeli;
        private System.Windows.Forms.TextBox txtCodemeli;
        private System.Windows.Forms.Label lblCridit;
        private System.Windows.Forms.TextBox txtCridit;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAcountBalance;
        private System.Windows.Forms.Label label2;
    }
}