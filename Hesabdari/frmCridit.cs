﻿using System;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmCridit : Form
    {
        public frmCridit()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCodemeli.Text != "" && txtName.Text != "" && txtCridit.Text != "")
                {
                    matinDataSet.customerDataTable ocustomerDataTable
                        = new matinDataSet.customerDataTable();

                    matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                        new matinDataSetTableAdapters.customerTableAdapter();

                    ocustomerTableAdapter.FillCustomerByCodemelli(ocustomerDataTable, txtCodemeli.Text);

                    if (ocustomerDataTable.Count != 1)
                    {
                        MessageBox.Show("مشتری مورد نظر پیدا نشد.");
                    }
                    else
                    {
                        matinDataSet.customerRow ocustomerRow = ocustomerDataTable[0];

                        matinDataSet.criditDataTable ocriditDataTable
                            = new matinDataSet.criditDataTable();

                        matinDataSetTableAdapters.criditTableAdapter ocriditTableAdapter =
                            new matinDataSetTableAdapters.criditTableAdapter();

                        ocriditTableAdapter.InsertCriditByCridits(Int32.Parse(txtCridit.Text), DateTime.Now, ocustomerRow.cid);

                        MessageBox.Show("شارژ حساب با موفقیت انجام شد.");
                    }
                }
                else
                {
                    MessageBox.Show("فیلدهای مورد نظر را پر نمایید!");
                }
            }
            catch { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtCodemeli.Text != "")
            {
                matinDataSet.customerDataTable ocustomerDataTable
                    = new matinDataSet.customerDataTable();

                matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                    new matinDataSetTableAdapters.customerTableAdapter();

                ocustomerTableAdapter.FillCustomerByCodemelli(ocustomerDataTable, txtCodemeli.Text);

                if (ocustomerDataTable.Count != 1)
                {
                    txtName.Text = "";
                    txtAcountBalance.Text = "";

                    MessageBox.Show("مشتری مورد نظر پیدا نشد.");
                }
                else
                {
                    matinDataSet.customerRow ocustomerRow = ocustomerDataTable[0];

                    txtName.Text = ocustomerRow.cname;

                    matinDataSetTableAdapters.factorTableAdapter ofactorTableAdapter =
                        new matinDataSetTableAdapters.factorTableAdapter();

                    matinDataSetTableAdapters.criditTableAdapter ocriditTableAdapter =
                        new matinDataSetTableAdapters.criditTableAdapter();

                    long? cridit_SumPrice = ocriditTableAdapter.SumPriceByCustomerID(ocustomerRow.cid);
                    long? factor_SumQuantity = ofactorTableAdapter.SumQuantityByCustomerID(ocustomerRow.cid);

                    if (cridit_SumPrice == null)
                    {
                        cridit_SumPrice = 0;
                    }

                    if (factor_SumQuantity == null)
                    {
                        factor_SumQuantity = 0;
                    }

                    txtAcountBalance.Text =
                        (cridit_SumPrice - factor_SumQuantity).ToString();
                }
            }
            else
            {
                MessageBox.Show("فیلدهای مورد نظر را پر نمایید!");
            }
        }
    }
}
