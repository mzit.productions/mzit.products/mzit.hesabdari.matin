﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmCustomer : Form
    {
        public frmCustomer()
        {
            InitializeComponent();
        }

        SqlConnection my_cn = new SqlConnection(" Server=(local);Database=matindb;Integrated Security=True; ");

        private DataView dvCustomerFilters = new DataView();

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtName.Text != "" && txtCodemeli.Text != "" && txtMobile.Text != "" &&
                    txtPhone.Text != "" && txtAddress.Text != "")
                {
                    matinDataSet.customerDataTable ocustomerDataTable
                               = new matinDataSet.customerDataTable();

                    matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                        new matinDataSetTableAdapters.customerTableAdapter();

                    if (!Properties.Settings.Default.isEditMode)
                    {
                        ocustomerTableAdapter.InsertCustomerByCustomers(
                            txtCodemeli.Text,
                            txtName.Text,
                            txtMobile.Text,
                            txtPhone.Text,
                            txtAddress.Text);

                        RefreshCustomers();
                        RefreshCustomerForm();
                        MessageBox.Show("مشتری جدید با موفقیت ذخیره شد.");
                    }
                    else
                    {
                        ocustomerTableAdapter.UpdateCustomerByCustomerID(
                            txtCodemeli.Text,
                            txtName.Text,
                            txtMobile.Text,
                            txtPhone.Text,
                            txtAddress.Text,
                            Int32.Parse(Properties.Settings.Default.ChoiceCustomerID),
                            Int32.Parse(Properties.Settings.Default.ChoiceCustomerID));

                        RefreshCustomers();
                        RefreshCustomerForm();
                        MessageBox.Show("مشتری با موفقیت ویرایش شد.");
                    }
                }
                else
                {
                    MessageBox.Show("فیلدهای مورد نظر را پر نمایید!");
                }
            }
            catch { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (!Properties.Settings.Default.isEditMode)
            {
                Close();
            }
            else
            {
                RefreshCustomerForm();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCodemeliCustomer.Text != "")
                {
                    matinDataSet.customerDataTable ocustomerDataTable
                        = new matinDataSet.customerDataTable();

                    matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                        new matinDataSetTableAdapters.customerTableAdapter();

                    ocustomerTableAdapter.FillCustomerByCodemelli(ocustomerDataTable, txtCodemeliCustomer.Text);

                    if (ocustomerDataTable.Count != 1)
                    {
                        MessageBox.Show("مشتری مورد نظر پیدا نشد.");
                    }
                    else
                    {
                        matinDataSet.customerRow ocustomerRow = ocustomerDataTable[0];

                        txtName.Text = ocustomerRow.cname;
                        txtCodemeli.Text = ocustomerRow.ccodemelli;
                        txtMobile.Text = ocustomerRow.cmobile;
                        txtPhone.Text = ocustomerRow.cphone;
                        txtAddress.Text = ocustomerRow.caddress;

                        MessageBox.Show("مشتری مورد نظر با موفقیت پیدا شد.");
                    }
                }
                else
                {
                    MessageBox.Show("کدملی مشتری را جهت جستجو وارد نمایید!");
                }
            }
            catch { }
        }

        private void txtNameCustomer_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtNameCustomer.Text != "")
                {
                    string Q_NameCustomerLike =
                        "SELECT cid, ccodemelli, cname, cmobile, cphone, caddress FROM customer WHERE cname LIKE '%" + txtNameCustomer.Text + "%'";

                    SqlCommand NameCustomerLikecmd = new SqlCommand(Q_NameCustomerLike, my_cn);

                    try
                    {
                        SqlDataAdapter ObjectAdapter = new SqlDataAdapter(NameCustomerLikecmd);
                        DataSet ds = new DataSet();
                        ObjectAdapter.Fill(ds, "customer");
                        dvCustomerFilters.Table = ds.Tables["customer"];

                        dgvCustomers.DataSource = dvCustomerFilters;

                        int index = -1;

                        dgvCustomers.Columns[++index].HeaderText = "شناسه مشتری";
                        dgvCustomers.Columns[++index].HeaderText = "کد ملی";
                        dgvCustomers.Columns[++index].HeaderText = "نام و نام خانوادگی";
                        dgvCustomers.Columns[++index].HeaderText = "تلفن همراه";
                        dgvCustomers.Columns[++index].HeaderText = "تلفن";
                        dgvCustomers.Columns[++index].HeaderText = "آدرس";
                    }
                    catch { }
                }
                else
                {
                    RefreshCustomers();
                }
            }
            catch { }
        }

        private bool RefreshCustomers()
        {
            try
            {
                matinDataSet.customerDataTable ocustomerDataTable
                        = new matinDataSet.customerDataTable();

                matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                    new matinDataSetTableAdapters.customerTableAdapter();

                ocustomerTableAdapter.FillCustomerByCustomers(ocustomerDataTable);

                if (ocustomerDataTable.Count != 0)
                {
                    dgvCustomers.DataSource = ocustomerDataTable;

                    int index = -1;

                    dgvCustomers.Columns[++index].HeaderText = "شناسه مشتری";
                    dgvCustomers.Columns[++index].HeaderText = "کد ملی";
                    dgvCustomers.Columns[++index].HeaderText = "نام و نام خانوادگی";
                    dgvCustomers.Columns[++index].HeaderText = "تلفن همراه";
                    dgvCustomers.Columns[++index].HeaderText = "تلفن";
                    dgvCustomers.Columns[++index].HeaderText = "آدرس";
                }
            }
            catch { }

            return true;
        }

        private void btnUpdateCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCustomers.SelectedRows[0].Cells[0].Value.ToString() != null && dgvCustomers.SelectedRows[0].Cells[0].Value.ToString() != "")
                {
                    if (MessageBox.Show("آیا میخواهید مشتری مورد نظر را ویرایش نمایید.",
                                       "ویرایش مشتری",
                                       MessageBoxButtons.YesNo,
                                       MessageBoxIcon.Information,
                                       MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Properties.Settings.Default.isEditMode = true;
                        Properties.Settings.Default.ChoiceCustomerID = dgvCustomers.SelectedRows[0].Cells[0].Value.ToString();

                        btnSave.Text = "ویرایش";

                        matinDataSet.customerDataTable ocustomerDataTable
                        = new matinDataSet.customerDataTable();

                        matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                            new matinDataSetTableAdapters.customerTableAdapter();

                        ocustomerTableAdapter.FillCustomerByID(ocustomerDataTable, Int32.Parse(Properties.Settings.Default.ChoiceCustomerID));

                        if (!(ocustomerDataTable.Count != 1))
                        {
                            matinDataSet.customerRow ocustomerRow = ocustomerDataTable[0];

                            txtName.Text = ocustomerRow.cname;
                            txtCodemeli.Text = ocustomerRow.ccodemelli;
                            txtMobile.Text = ocustomerRow.cmobile;
                            txtPhone.Text = ocustomerRow.cphone;
                            txtAddress.Text = ocustomerRow.caddress;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("مشتری مورد نظر را جهت ویرایش انتخاب کنید.",
                                    "خطا",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error,
                                    MessageBoxDefaultButton.Button1);
                }
            }
            catch { }
        }

        private void btnDeleteCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvCustomers.SelectedRows[0].Cells[0].Value.ToString() != null && dgvCustomers.SelectedRows[0].Cells[0].Value.ToString() != "")
                {
                    if (MessageBox.Show("آیا میخواهید مشتری مورد نظر را حذف نمایید.",
                                       "حذف مشتری",
                                       MessageBoxButtons.YesNo,
                                       MessageBoxIcon.Warning,
                                       MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        matinDataSet.customerDataTable ocustomerDataTable
                        = new matinDataSet.customerDataTable();

                        matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                            new matinDataSetTableAdapters.customerTableAdapter();

                        ocustomerTableAdapter.DeleteCustomerByCustomerID(Int32.Parse(dgvCustomers.SelectedRows[0].Cells[0].Value.ToString()));

                        RefreshCustomers();

                        MessageBox.Show("مشتری مورد نظر با موفقیت حذف شد.",
                                        "حذف مشتری",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information,
                                        MessageBoxDefaultButton.Button1);
                    }
                }
                else
                {
                    MessageBox.Show("مشتری مورد نظر را جهت حذف انتخاب کنید.",
                                    "خطا",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error,
                                    MessageBoxDefaultButton.Button1);
                }
            }
            catch
            {
                MessageBox.Show("حذف مشتری مورد نظر با خطا همراه بود." + "\n" +
                                "لطفا از عدم استفاده‌ی مشتری مورد نظر در سفارش‌های موجود و شارژ حساب، اطمینان حاصل نمایید.",
                                "خطا",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error,
                                MessageBoxDefaultButton.Button1);
            }
        }

        private void RefreshCustomerForm()
        {
            try
            {
                txtCodemeli.Text = "";
                txtName.Text = "";
                txtMobile.Text = "";
                txtPhone.Text = "";
                txtAddress.Text = "";

                btnSave.Text = "ثبت";

                Properties.Settings.Default.isEditMode = false;
            }
            catch { }
        }

        private void frmCustomer_Load(object sender, EventArgs e)
        {
            try
            {
                RefreshCustomerForm();
                RefreshCustomers();
                txtCodemeliCustomer.Text = "";
                txtNameCustomer.Text = "";
            }
            catch { }
        }
    }
}
