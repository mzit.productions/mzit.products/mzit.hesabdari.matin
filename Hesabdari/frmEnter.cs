﻿using System;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmEnter : Form
    {
        public frmEnter()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUsername.Text != "" || txtPass.Text != "")
                {
                    matinDataSet.userDataTable ouserDataTable
                        = new matinDataSet.userDataTable();

                    matinDataSetTableAdapters.userTableAdapter ouserTableAdapter =
                        new matinDataSetTableAdapters.userTableAdapter();

                    if (ouserTableAdapter.CountUserByPassUser(txtUsername.Text, txtPass.Text) != 1)
                    {
                        MessageBox.Show("نام کاربری یا رمز ورود اشتباه است.");
                    }
                    else
                    {
                        DialogResult = DialogResult.OK;
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("فیلدهای مورد نظر را پر نمایید!");
                }
            }
            catch { }
        }
    }
}
