﻿using System;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOrder_Click(object sender, EventArgs e)
        {
            try
            {
                frmOrder _frmOrder = new frmOrder();

                this.Hide();
                _frmOrder.ShowDialog();
                this.Show();
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                frmCustomer _frmCustomer = new frmCustomer();

                this.Hide();
                _frmCustomer.ShowDialog();
                this.Show();
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnProduct_Click(object sender, EventArgs e)
        {
            try
            {
                frmProduct _frmProduct = new frmProduct();

                this.Hide();
                _frmProduct.ShowDialog();
                this.Show();
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            try
            {
                frmReport _frmReport = new frmReport();

                this.Hide();
                _frmReport.ShowDialog();
                this.Show();
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnCridit_Click(object sender, EventArgs e)
        {
            try
            {
                frmCridit _frmCridit = new frmCridit();

                this.Hide();
                _frmCridit.ShowDialog();
                this.Show();
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnChangePass_Click(object sender, EventArgs e)
        {
            try
            {
                frmChangepass _frmChangepass = new frmChangepass();

                this.Hide();
                _frmChangepass.ShowDialog();
                this.Show();
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

    }
}
