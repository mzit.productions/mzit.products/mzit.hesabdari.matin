﻿namespace Hesabdari
{
    partial class frmOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvShoppingCart = new System.Windows.Forms.DataGridView();
            this.rowOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDeleteOrderRow = new System.Windows.Forms.DataGridViewButtonColumn();
            this.numericUpDownProduct = new System.Windows.Forms.NumericUpDown();
            this.btnAddProduct = new System.Windows.Forms.Button();
            this.dateTimePickerOrderTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNameCustomer = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnChoiceCustomer = new System.Windows.Forms.Button();
            this.btnChoiceProduct = new System.Windows.Forms.Button();
            this.lblNameProduct = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShoppingCart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvShoppingCart
            // 
            this.dgvShoppingCart.AllowUserToAddRows = false;
            this.dgvShoppingCart.AllowUserToDeleteRows = false;
            this.dgvShoppingCart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvShoppingCart.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvShoppingCart.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvShoppingCart.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvShoppingCart.ColumnHeadersHeight = 26;
            this.dgvShoppingCart.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rowOrder,
            this.pId,
            this.pCode,
            this.pName,
            this.pPrice,
            this.oCount,
            this.sumPrice,
            this.btnDeleteOrderRow});
            this.dgvShoppingCart.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvShoppingCart.Location = new System.Drawing.Point(12, 204);
            this.dgvShoppingCart.MultiSelect = false;
            this.dgvShoppingCart.Name = "dgvShoppingCart";
            this.dgvShoppingCart.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShoppingCart.Size = new System.Drawing.Size(563, 179);
            this.dgvShoppingCart.TabIndex = 13;
            this.dgvShoppingCart.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvShoppingCart_CellContentClick);
            // 
            // rowOrder
            // 
            this.rowOrder.HeaderText = "ردیف";
            this.rowOrder.Name = "rowOrder";
            this.rowOrder.ReadOnly = true;
            this.rowOrder.Width = 55;
            // 
            // pId
            // 
            this.pId.HeaderText = "شناسه کالا";
            this.pId.Name = "pId";
            this.pId.ReadOnly = true;
            this.pId.Width = 83;
            // 
            // pCode
            // 
            this.pCode.HeaderText = "کد کالا";
            this.pCode.Name = "pCode";
            this.pCode.ReadOnly = true;
            this.pCode.Width = 64;
            // 
            // pName
            // 
            this.pName.HeaderText = "نام کالا";
            this.pName.Name = "pName";
            this.pName.ReadOnly = true;
            this.pName.Width = 65;
            // 
            // pPrice
            // 
            this.pPrice.HeaderText = "قیمت کالا";
            this.pPrice.Name = "pPrice";
            this.pPrice.ReadOnly = true;
            this.pPrice.Width = 75;
            // 
            // oCount
            // 
            this.oCount.HeaderText = "تعداد کالا";
            this.oCount.Name = "oCount";
            this.oCount.Width = 75;
            // 
            // sumPrice
            // 
            this.sumPrice.HeaderText = "مجموع قیمت";
            this.sumPrice.Name = "sumPrice";
            this.sumPrice.ReadOnly = true;
            this.sumPrice.Width = 88;
            // 
            // btnDeleteOrderRow
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            this.btnDeleteOrderRow.DefaultCellStyle = dataGridViewCellStyle2;
            this.btnDeleteOrderRow.HeaderText = "حذف";
            this.btnDeleteOrderRow.Name = "btnDeleteOrderRow";
            this.btnDeleteOrderRow.Width = 34;
            // 
            // numericUpDownProduct
            // 
            this.numericUpDownProduct.Location = new System.Drawing.Point(131, 175);
            this.numericUpDownProduct.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownProduct.Name = "numericUpDownProduct";
            this.numericUpDownProduct.Size = new System.Drawing.Size(55, 20);
            this.numericUpDownProduct.TabIndex = 15;
            this.numericUpDownProduct.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.Location = new System.Drawing.Point(192, 174);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(113, 24);
            this.btnAddProduct.TabIndex = 16;
            this.btnAddProduct.Text = "اضافه به سبد خرید";
            this.btnAddProduct.UseVisualStyleBackColor = true;
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // dateTimePickerOrderTime
            // 
            this.dateTimePickerOrderTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerOrderTime.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerOrderTime.Location = new System.Drawing.Point(455, 98);
            this.dateTimePickerOrderTime.Name = "dateTimePickerOrderTime";
            this.dateTimePickerOrderTime.Size = new System.Drawing.Size(120, 20);
            this.dateTimePickerOrderTime.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 29);
            this.label2.TabIndex = 20;
            this.label2.Text = "ثبت سفارش";
            // 
            // lblNameCustomer
            // 
            this.lblNameCustomer.AutoSize = true;
            this.lblNameCustomer.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameCustomer.Location = new System.Drawing.Point(138, 98);
            this.lblNameCustomer.Name = "lblNameCustomer";
            this.lblNameCustomer.Size = new System.Drawing.Size(24, 19);
            this.lblNameCustomer.TabIndex = 21;
            this.lblNameCustomer.Text = "...";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(531, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 19);
            this.label1.TabIndex = 22;
            this.label1.Text = "تاریخ";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(12, 402);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 34);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "ثبت";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(192, 402);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(84, 34);
            this.btnCancel.TabIndex = 24;
            this.btnCancel.Text = "لغو";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblQuantity
            // 
            this.lblQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuantity.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantity.Location = new System.Drawing.Point(383, 172);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(192, 19);
            this.lblQuantity.TabIndex = 26;
            this.lblQuantity.Text = "0";
            this.lblQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(505, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 19);
            this.label4.TabIndex = 27;
            this.label4.Text = "جمع کل";
            // 
            // btnChoiceCustomer
            // 
            this.btnChoiceCustomer.Location = new System.Drawing.Point(16, 97);
            this.btnChoiceCustomer.Name = "btnChoiceCustomer";
            this.btnChoiceCustomer.Size = new System.Drawing.Size(113, 24);
            this.btnChoiceCustomer.TabIndex = 28;
            this.btnChoiceCustomer.Text = "انتخاب مشتری ...";
            this.btnChoiceCustomer.UseVisualStyleBackColor = true;
            this.btnChoiceCustomer.Click += new System.EventHandler(this.btnChoiceCustomer_Click);
            // 
            // btnChoiceProduct
            // 
            this.btnChoiceProduct.Location = new System.Drawing.Point(16, 142);
            this.btnChoiceProduct.Name = "btnChoiceProduct";
            this.btnChoiceProduct.Size = new System.Drawing.Size(113, 24);
            this.btnChoiceProduct.TabIndex = 29;
            this.btnChoiceProduct.Text = "انتخاب کالا ...";
            this.btnChoiceProduct.UseVisualStyleBackColor = true;
            this.btnChoiceProduct.Click += new System.EventHandler(this.btnChoiceProduct_Click);
            // 
            // lblNameProduct
            // 
            this.lblNameProduct.AutoSize = true;
            this.lblNameProduct.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameProduct.Location = new System.Drawing.Point(138, 143);
            this.lblNameProduct.Name = "lblNameProduct";
            this.lblNameProduct.Size = new System.Drawing.Size(24, 19);
            this.lblNameProduct.TabIndex = 30;
            this.lblNameProduct.Text = "...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(84, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 19);
            this.label5.TabIndex = 31;
            this.label5.Text = "تعداد";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefresh.Location = new System.Drawing.Point(102, 402);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(84, 34);
            this.btnRefresh.TabIndex = 32;
            this.btnRefresh.Text = "تازه سازی";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // frmOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumTurquoise;
            this.ClientSize = new System.Drawing.Size(587, 448);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblNameProduct);
            this.Controls.Add(this.btnChoiceProduct);
            this.Controls.Add(this.btnChoiceCustomer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblQuantity);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNameCustomer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePickerOrderTime);
            this.Controls.Add(this.btnAddProduct);
            this.Controls.Add(this.numericUpDownProduct);
            this.Controls.Add(this.dgvShoppingCart);
            this.Name = "frmOrder";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.Text = "ثبت سفارش";
            this.Load += new System.EventHandler(this.frmOrder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShoppingCart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownProduct)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvShoppingCart;
        private System.Windows.Forms.NumericUpDown numericUpDownProduct;
        private System.Windows.Forms.Button btnAddProduct;
        private System.Windows.Forms.DateTimePicker dateTimePickerOrderTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNameCustomer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnChoiceCustomer;
        private System.Windows.Forms.Button btnChoiceProduct;
        private System.Windows.Forms.Label lblNameProduct;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn pId;
        private System.Windows.Forms.DataGridViewTextBoxColumn pCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn pName;
        private System.Windows.Forms.DataGridViewTextBoxColumn pPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn oCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumPrice;
        private System.Windows.Forms.DataGridViewButtonColumn btnDeleteOrderRow;
    }
}