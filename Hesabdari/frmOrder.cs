﻿using System;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmOrder : Form
    {
        public frmOrder()
        {
            InitializeComponent();
        }

        private bool LoadChoiceCustomer(string strCustomerID)
        {
            try
            {
                if (strCustomerID != "")
                {
                    matinDataSet.customerDataTable ocustomerDataTable
                        = new matinDataSet.customerDataTable();

                    matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                        new matinDataSetTableAdapters.customerTableAdapter();

                    ocustomerTableAdapter.FillCustomerByID(ocustomerDataTable, Int32.Parse(strCustomerID));

                    if (ocustomerDataTable.Count != 1)
                    {
                        //MessageBox.Show("کالای مورد نظر پیدا نشد.");
                    }
                    else
                    {
                        matinDataSet.customerRow ocustomerRow = ocustomerDataTable[0];

                        lblNameCustomer.Text = ocustomerRow.cname;
                    }
                }
                else
                {
                    lblNameCustomer.Text = "...";
                }
            }
            catch { }

            return true;
        }

        private void btnChoiceCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.ChoiceCustomerID = string.Empty;

                frmChoiceCustomer _frmChoiceCustomer = new frmChoiceCustomer();
                _frmChoiceCustomer.ShowDialog();

                LoadChoiceCustomer(Properties.Settings.Default.ChoiceCustomerID);
            }
            catch
            {
                Cursor = Cursors.Default;
            }
        }

        private bool LoadChoiceProduct(string strProductID)
        {
            try
            {
                if (strProductID != "")
                {
                    matinDataSet.productDataTable oproductDataTable
                        = new matinDataSet.productDataTable();

                    matinDataSetTableAdapters.productTableAdapter oproductTableAdapter =
                        new matinDataSetTableAdapters.productTableAdapter();

                    oproductTableAdapter.FillProductByID(oproductDataTable, Int32.Parse(strProductID));

                    if (oproductDataTable.Count != 1)
                    {
                        //MessageBox.Show("کالای مورد نظر پیدا نشد.");
                    }
                    else
                    {
                        matinDataSet.productRow oproductRow = oproductDataTable[0];

                        lblNameProduct.Text = oproductRow.pname;
                    }
                }
                else
                {
                    lblNameProduct.Text = "...";
                }
            }
            catch { }

            return true;
        }

        private void btnChoiceProduct_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.ChoiceProductID = string.Empty;

                frmChoiceProduct _frmChoiceProduct = new frmChoiceProduct();
                _frmChoiceProduct.ShowDialog();

                LoadChoiceProduct(Properties.Settings.Default.ChoiceProductID);
            }
            catch
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            try
            {
                if (Properties.Settings.Default.ChoiceProductID != "" && numericUpDownProduct.Value > 0)
                {
                    matinDataSet.productDataTable oproductDataTable
                        = new matinDataSet.productDataTable();

                    matinDataSetTableAdapters.productTableAdapter oproductTableAdapter =
                        new matinDataSetTableAdapters.productTableAdapter();

                    oproductTableAdapter.FillProductByID(oproductDataTable, Int32.Parse(Properties.Settings.Default.ChoiceProductID));

                    if (oproductDataTable.Count != 1)
                    {
                        //MessageBox.Show("کالای مورد نظر پیدا نشد.");
                    }
                    else
                    {
                        matinDataSet.productRow oproductRow = oproductDataTable[0];

                        DataGridViewRow newOrder = new DataGridViewRow();
                        newOrder.CreateCells(dgvShoppingCart);

                        int index = -1;

                        newOrder.Cells[++index].Value = ++Properties.Settings.Default.OrderRow;
                        newOrder.Cells[++index].Value = oproductRow.pid;
                        newOrder.Cells[++index].Value = oproductRow.pcode;
                        newOrder.Cells[++index].Value = oproductRow.pname;
                        newOrder.Cells[++index].Value = oproductRow.pprice;
                        newOrder.Cells[++index].Value = numericUpDownProduct.Value;
                        newOrder.Cells[++index].Value = (numericUpDownProduct.Value * oproductRow.pprice);

                        dgvShoppingCart.Rows.Add(newOrder);

                        lblQuantity.Text = (Int32.Parse(lblQuantity.Text) + (numericUpDownProduct.Value * oproductRow.pprice)).ToString();
                    }
                }
            }
            catch { }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Properties.Settings.Default.ChoiceCustomerID != "")
                {
                    if (dgvShoppingCart.RowCount > 0)
                    {
                        matinDataSet.factorDataTable ofactorDataTable
                        = new matinDataSet.factorDataTable();

                        matinDataSetTableAdapters.factorTableAdapter ofactorTableAdapter =
                            new matinDataSetTableAdapters.factorTableAdapter();

                        int lastFactorIdRecord = -1;

                        ofactorTableAdapter.InsertFactorByFactors(dateTimePickerOrderTime.Value, Int32.Parse(lblQuantity.Text), Int32.Parse(Properties.Settings.Default.ChoiceCustomerID));

                        if (ofactorTableAdapter.TopFactorIdRecorder() != null)
                        {
                            lastFactorIdRecord = Int32.Parse(ofactorTableAdapter.TopFactorIdRecorder().ToString());
                        }

                        for (int index = 0; index < dgvShoppingCart.RowCount; index++)
                        {
                            matinDataSet.orderDataTable oorderDataTable =
                                new matinDataSet.orderDataTable();

                            matinDataSetTableAdapters.orderTableAdapter oorderTableAdapter =
                                new matinDataSetTableAdapters.orderTableAdapter();

                            oorderTableAdapter.InsertOrderByOrders(lastFactorIdRecord, Int32.Parse(dgvShoppingCart.Rows[index].Cells[1].Value.ToString()), Int32.Parse(dgvShoppingCart.Rows[index].Cells[5].Value.ToString()));
                        }

                        RefreshOrderForm();
                        MessageBox.Show("سفارش جدید با موفقیت ثبت شد.");
                    }
                    else
                    {
                        MessageBox.Show("کالاهای مورد نظر را انتخاب نمایید!");
                    }
                }
                else
                {
                    MessageBox.Show("مشتری مورد نظر را انتخاب نمایید!");
                }
            }
            catch
            {
                MessageBox.Show("ثبت سفارش با خطا همراه بود!");
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshOrderForm();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool RefreshOrderForm()
        {
            try
            {
                lblNameCustomer.Text = "...";
                lblNameProduct.Text = "...";
                numericUpDownProduct.Value = 1;
                lblQuantity.Text = "0";
                dateTimePickerOrderTime.Value = DateTime.Now;
                dgvShoppingCart.Rows.Clear();

                Properties.Settings.Default.ChoiceCustomerID = string.Empty;
                Properties.Settings.Default.ChoiceProductID = string.Empty;
                Properties.Settings.Default.OrderRow = 0;
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void frmOrder_Load(object sender, EventArgs e)
        {
            try
            {
                RefreshOrderForm();
            }
            catch { }
        }

        private void dgvShoppingCart_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                lblQuantity.Text = (Int32.Parse(lblQuantity.Text) - (Int32.Parse(dgvShoppingCart.Rows[e.RowIndex].Cells[6].Value.ToString()))).ToString();
                dgvShoppingCart.Rows.RemoveAt(e.RowIndex);
            }
        }
    }
}
