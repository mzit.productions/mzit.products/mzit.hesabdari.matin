﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmProduct : Form
    {
        public frmProduct()
        {
            InitializeComponent();
        }

        SqlConnection my_cn = new SqlConnection(" Server=(local);Database=matindb;Integrated Security=True; ");

        private DataView dvProductFilters = new DataView();

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCode.Text != "" && txtName.Text != "" && txtPrice.Text != "")
                {
                    matinDataSet.productDataTable oproductDataTable
                        = new matinDataSet.productDataTable();

                    matinDataSetTableAdapters.productTableAdapter oproductTableAdapter =
                        new matinDataSetTableAdapters.productTableAdapter();

                    if (!Properties.Settings.Default.isEditMode)
                    {
                        oproductTableAdapter.InsertProductByProducts(
                            Int32.Parse(txtCode.Text),
                            txtName.Text,
                            Int32.Parse(txtPrice.Text));

                        RefreshProducts();
                        RefreshProductForm();
                        MessageBox.Show("کالای جدید با موفقیت ذخیره شد.");
                    }
                    else
                    {
                        oproductTableAdapter.UpdateProductByProductID(
                            Int32.Parse(txtCode.Text),
                            txtName.Text,
                            Int32.Parse(txtPrice.Text),
                            Int32.Parse(Properties.Settings.Default.ChoiceProductID),
                            Int32.Parse(Properties.Settings.Default.ChoiceProductID));

                        RefreshProducts();
                        RefreshProductForm();
                        MessageBox.Show("کالا با موفقیت ویرایش شد.");
                    }
                }
                else
                {
                    MessageBox.Show("فیلدهای مورد نظر را پر نمایید!");
                }
            }
            catch { }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (!Properties.Settings.Default.isEditMode)
            {
                Close();
            }
            else
            {
                RefreshProductForm();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCodekala.Text != "")
                {
                    matinDataSet.productDataTable oproductDataTable
                        = new matinDataSet.productDataTable();

                    matinDataSetTableAdapters.productTableAdapter oproductTableAdapter =
                        new matinDataSetTableAdapters.productTableAdapter();

                    oproductTableAdapter.FillProductByCode(oproductDataTable, Int32.Parse(txtCodekala.Text));

                    if (oproductDataTable.Count != 1)
                    {
                        MessageBox.Show("کالای مورد نظر پیدا نشد.");
                    }
                    else
                    {
                        matinDataSet.productRow oproductRow = oproductDataTable[0];

                        txtCode.Text = oproductRow.pcode.ToString();
                        txtName.Text = oproductRow.pname;
                        txtPrice.Text = oproductRow.pprice.ToString();

                        MessageBox.Show("کالای مورد نظر با موفقیت پیدا شد.");
                    }
                }
                else
                {
                    MessageBox.Show("کد کالا را جهت جستجو وارد نمایید!");
                }
            }
            catch { }
        }

        private void txtNameKala_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtNameKala.Text != "")
                {
                    string Q_NameKalaLike =
                        "SELECT pid, pcode, pname, pprice FROM product WHERE pname LIKE '%" + txtNameKala.Text + "%'";
                    //"SELECT cid, ccodemelli, cname, cmobile, cphone, caddress FROM customer WHERE (cname LIKE '%@cnameLike%')";

                    SqlCommand NameKalaLikecmd = new SqlCommand(Q_NameKalaLike, my_cn);

                    try
                    {
                        SqlDataAdapter ObjectAdapter = new SqlDataAdapter(NameKalaLikecmd);
                        DataSet ds = new DataSet();
                        ObjectAdapter.Fill(ds, "product");
                        dvProductFilters.Table = ds.Tables["product"];

                        dgvProducts.DataSource = dvProductFilters;

                        int index = -1;

                        dgvProducts.Columns[++index].HeaderText = "شناسه کالا";
                        dgvProducts.Columns[++index].HeaderText = "کد کالا";
                        dgvProducts.Columns[++index].HeaderText = "نام کالا";
                        dgvProducts.Columns[++index].HeaderText = "قیمت کالا";
                    }
                    catch { }
                }
                else
                {
                    RefreshProducts();
                }
            }
            catch { }
        }

        private bool RefreshProducts()
        {
            try
            {
                matinDataSet.productDataTable oproductDataTable
                        = new matinDataSet.productDataTable();

                matinDataSetTableAdapters.productTableAdapter oproductTableAdapter =
                    new matinDataSetTableAdapters.productTableAdapter();

                oproductTableAdapter.FillProductByProducts(oproductDataTable);

                if (oproductDataTable.Count != 0)
                {
                    dgvProducts.DataSource = oproductDataTable;

                    int index = -1;

                    dgvProducts.Columns[++index].HeaderText = "شناسه کالا";
                    dgvProducts.Columns[++index].HeaderText = "کد کالا";
                    dgvProducts.Columns[++index].HeaderText = "نام کالا";
                    dgvProducts.Columns[++index].HeaderText = "قیمت کالا";
                }
            }
            catch { }

            return true;
        }

        private void btnUpdateProduct_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvProducts.SelectedRows[0].Cells[0].Value.ToString() != null && dgvProducts.SelectedRows[0].Cells[0].Value.ToString() != "")
                {
                    if (MessageBox.Show("آیا میخواهید کالای مورد نظر را ویرایش نمایید.",
                                       "ویرایش کالا",
                                       MessageBoxButtons.YesNo,
                                       MessageBoxIcon.Information,
                                       MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Properties.Settings.Default.isEditMode = true;
                        Properties.Settings.Default.ChoiceProductID = dgvProducts.SelectedRows[0].Cells[0].Value.ToString();

                        btnSave.Text = "ویرایش";

                        matinDataSet.productDataTable oproductDataTable
                        = new matinDataSet.productDataTable();

                        matinDataSetTableAdapters.productTableAdapter oproductTableAdapter =
                            new matinDataSetTableAdapters.productTableAdapter();

                        oproductTableAdapter.FillProductByID(oproductDataTable, Int32.Parse(Properties.Settings.Default.ChoiceCustomerID));

                        if (!(oproductDataTable.Count != 1))
                        {
                            matinDataSet.productRow oproductRow = oproductDataTable[0];

                            txtCode.Text = oproductRow.pcode.ToString();
                            txtName.Text = oproductRow.pname;
                            txtPrice.Text = oproductRow.pprice.ToString();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("کالای مورد نظر را جهت ویرایش انتخاب کنید.",
                                    "خطا",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error,
                                    MessageBoxDefaultButton.Button1);
                }
            }
            catch { }
        }

        private void btnDeleteProduct_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvProducts.SelectedRows[0].Cells[0].Value.ToString() != null && dgvProducts.SelectedRows[0].Cells[0].Value.ToString() != "")
                {
                    if (MessageBox.Show("آیا میخواهید کالای مورد نظر را حذف نمایید.",
                                       "حذف کالا",
                                       MessageBoxButtons.YesNo,
                                       MessageBoxIcon.Warning,
                                       MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        matinDataSet.productDataTable oproductDataTable
                        = new matinDataSet.productDataTable();

                        matinDataSetTableAdapters.productTableAdapter oproductTableAdapter =
                            new matinDataSetTableAdapters.productTableAdapter();

                        oproductTableAdapter.DeleteProductByProductID(Int32.Parse(dgvProducts.SelectedRows[0].Cells[0].Value.ToString()));

                        RefreshProducts();

                        MessageBox.Show("کالای مورد نظر با موفقیت حذف شد.",
                                        "حذف کالا",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information,
                                        MessageBoxDefaultButton.Button1);
                    }
                }
                else
                {
                    MessageBox.Show("کالای مورد نظر را جهت حذف انتخاب کنید.",
                                    "خطا",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error,
                                    MessageBoxDefaultButton.Button1);
                }
            }
            catch
            {
                MessageBox.Show("حذف کالای مورد نظر با خطا همراه بود." + "\n" +
                                "لطفا از عدم استفاده‌ی کالای مورد نظر در سفارش‌های موجود، اطمینان حاصل نمایید.",
                                "خطا",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error,
                                MessageBoxDefaultButton.Button1);
            }
        }

        private void RefreshProductForm()
        {
            try
            {
                txtCode.Text = "";
                txtName.Text = "";
                txtPrice.Text = "";

                btnSave.Text = "ثبت";

                Properties.Settings.Default.isEditMode = false;
            }
            catch { }
        }

        private void frmProduct_Load(object sender, EventArgs e)
        {
            try
            {
                RefreshProductForm();
                RefreshProducts();
                txtCodekala.Text = "";
            }
            catch { }
        }
    }
}
