﻿namespace Hesabdari
{
    partial class frmReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCustomers = new System.Windows.Forms.DataGridView();
            this.chbSelectRowCostomer = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.rowCostomer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCodeMelli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cMobile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnDeselectAll = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerOrderTime_Start = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerOrderTime_End = new System.Windows.Forms.DateTimePicker();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnFactors = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dgvReports = new System.Windows.Forms.DataGridView();
            this.btnIncreaseCredit = new System.Windows.Forms.Button();
            this.btnAccountBalance = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReports)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCustomers
            // 
            this.dgvCustomers.AllowUserToAddRows = false;
            this.dgvCustomers.AllowUserToDeleteRows = false;
            this.dgvCustomers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCustomers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCustomers.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvCustomers.ColumnHeadersHeight = 26;
            this.dgvCustomers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chbSelectRowCostomer,
            this.rowCostomer,
            this.cId,
            this.cCodeMelli,
            this.cName,
            this.cMobile,
            this.cPhone,
            this.cAddress});
            this.dgvCustomers.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvCustomers.Location = new System.Drawing.Point(12, 63);
            this.dgvCustomers.Name = "dgvCustomers";
            this.dgvCustomers.ReadOnly = true;
            this.dgvCustomers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCustomers.Size = new System.Drawing.Size(572, 133);
            this.dgvCustomers.TabIndex = 28;
            this.dgvCustomers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCustomers_CellContentClick);
            // 
            // chbSelectRowCostomer
            // 
            this.chbSelectRowCostomer.HeaderText = "انتخاب";
            this.chbSelectRowCostomer.Name = "chbSelectRowCostomer";
            this.chbSelectRowCostomer.ReadOnly = true;
            this.chbSelectRowCostomer.Width = 43;
            // 
            // rowCostomer
            // 
            this.rowCostomer.HeaderText = "ردیف";
            this.rowCostomer.Name = "rowCostomer";
            this.rowCostomer.ReadOnly = true;
            this.rowCostomer.Width = 55;
            // 
            // cId
            // 
            this.cId.HeaderText = "شناسه مشتری";
            this.cId.Name = "cId";
            this.cId.ReadOnly = true;
            this.cId.Width = 99;
            // 
            // cCodeMelli
            // 
            this.cCodeMelli.HeaderText = "کد ملی";
            this.cCodeMelli.Name = "cCodeMelli";
            this.cCodeMelli.ReadOnly = true;
            this.cCodeMelli.Width = 65;
            // 
            // cName
            // 
            this.cName.HeaderText = "نام و نام خانوادگی";
            this.cName.Name = "cName";
            this.cName.ReadOnly = true;
            this.cName.Width = 119;
            // 
            // cMobile
            // 
            this.cMobile.HeaderText = "تلفن همراه";
            this.cMobile.Name = "cMobile";
            this.cMobile.ReadOnly = true;
            this.cMobile.Width = 80;
            // 
            // cPhone
            // 
            this.cPhone.HeaderText = "تلفن";
            this.cPhone.Name = "cPhone";
            this.cPhone.ReadOnly = true;
            this.cPhone.Width = 51;
            // 
            // cAddress
            // 
            this.cAddress.HeaderText = "آدرس";
            this.cAddress.Name = "cAddress";
            this.cAddress.ReadOnly = true;
            this.cAddress.Width = 55;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 19);
            this.label3.TabIndex = 29;
            this.label3.Text = "انتخاب مشتری ...";
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(12, 222);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(110, 34);
            this.btnSelectAll.TabIndex = 32;
            this.btnSelectAll.Text = "انتخاب همه";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnDeselectAll
            // 
            this.btnDeselectAll.Location = new System.Drawing.Point(128, 222);
            this.btnDeselectAll.Name = "btnDeselectAll";
            this.btnDeselectAll.Size = new System.Drawing.Size(110, 34);
            this.btnDeselectAll.TabIndex = 31;
            this.btnDeselectAll.Text = "غیرفعال کردن همه";
            this.btnDeselectAll.UseVisualStyleBackColor = true;
            this.btnDeselectAll.Click += new System.EventHandler(this.btnDeselectAll_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 279);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 19);
            this.label1.TabIndex = 34;
            this.label1.Text = "از تاریخ";
            // 
            // dateTimePickerOrderTime_Start
            // 
            this.dateTimePickerOrderTime_Start.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerOrderTime_Start.Location = new System.Drawing.Point(78, 278);
            this.dateTimePickerOrderTime_Start.Name = "dateTimePickerOrderTime_Start";
            this.dateTimePickerOrderTime_Start.Size = new System.Drawing.Size(137, 20);
            this.dateTimePickerOrderTime_Start.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(270, 279);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 19);
            this.label2.TabIndex = 36;
            this.label2.Text = "تا تاریخ";
            // 
            // dateTimePickerOrderTime_End
            // 
            this.dateTimePickerOrderTime_End.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerOrderTime_End.Location = new System.Drawing.Point(336, 278);
            this.dateTimePickerOrderTime_End.Name = "dateTimePickerOrderTime_End";
            this.dateTimePickerOrderTime_End.Size = new System.Drawing.Size(137, 20);
            this.dateTimePickerOrderTime_End.TabIndex = 35;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(318, 319);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(96, 34);
            this.btnRefresh.TabIndex = 39;
            this.btnRefresh.Text = "تازه سازی";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnFactors
            // 
            this.btnFactors.Location = new System.Drawing.Point(12, 319);
            this.btnFactors.Name = "btnFactors";
            this.btnFactors.Size = new System.Drawing.Size(96, 34);
            this.btnFactors.TabIndex = 38;
            this.btnFactors.Text = "فاکتورها";
            this.btnFactors.UseVisualStyleBackColor = true;
            this.btnFactors.Click += new System.EventHandler(this.btnFactors_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(420, 319);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 34);
            this.btnCancel.TabIndex = 37;
            this.btnCancel.Text = "لغو";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dgvReports
            // 
            this.dgvReports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReports.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvReports.BackgroundColor = System.Drawing.Color.Azure;
            this.dgvReports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReports.Location = new System.Drawing.Point(12, 379);
            this.dgvReports.MultiSelect = false;
            this.dgvReports.Name = "dgvReports";
            this.dgvReports.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReports.Size = new System.Drawing.Size(572, 190);
            this.dgvReports.TabIndex = 40;
            // 
            // btnIncreaseCredit
            // 
            this.btnIncreaseCredit.Location = new System.Drawing.Point(114, 319);
            this.btnIncreaseCredit.Name = "btnIncreaseCredit";
            this.btnIncreaseCredit.Size = new System.Drawing.Size(96, 34);
            this.btnIncreaseCredit.TabIndex = 41;
            this.btnIncreaseCredit.Text = "افزایش اعتبارها";
            this.btnIncreaseCredit.UseVisualStyleBackColor = true;
            this.btnIncreaseCredit.Click += new System.EventHandler(this.btnIncreaseCredit_Click);
            // 
            // btnAccountBalance
            // 
            this.btnAccountBalance.Location = new System.Drawing.Point(216, 319);
            this.btnAccountBalance.Name = "btnAccountBalance";
            this.btnAccountBalance.Size = new System.Drawing.Size(96, 34);
            this.btnAccountBalance.TabIndex = 42;
            this.btnAccountBalance.Text = "موجودی حساب‌ها";
            this.btnAccountBalance.UseVisualStyleBackColor = true;
            this.btnAccountBalance.Click += new System.EventHandler(this.btnAccountBalance_Click);
            // 
            // frmReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Plum;
            this.ClientSize = new System.Drawing.Size(596, 581);
            this.Controls.Add(this.btnAccountBalance);
            this.Controls.Add(this.btnIncreaseCredit);
            this.Controls.Add(this.dgvReports);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnFactors);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePickerOrderTime_End);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePickerOrderTime_Start);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnDeselectAll);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dgvCustomers);
            this.Name = "frmReport";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.Text = "گزارشگیری";
            this.Load += new System.EventHandler(this.frmReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReports)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCustomers;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnDeselectAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerOrderTime_Start;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePickerOrderTime_End;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnFactors;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chbSelectRowCostomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowCostomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn cId;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCodeMelli;
        private System.Windows.Forms.DataGridViewTextBoxColumn cName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cMobile;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAddress;
        private System.Windows.Forms.DataGridView dgvReports;
        private System.Windows.Forms.Button btnIncreaseCredit;
        private System.Windows.Forms.Button btnAccountBalance;
    }
}