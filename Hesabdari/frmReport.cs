﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Hesabdari
{
    public partial class frmReport : Form
    {
        public frmReport()
        {
            InitializeComponent();
        }

        SqlConnection my_cn = new SqlConnection(" Server=(local);Database=matindb;Integrated Security=True; ");

        private DataView dvReportFilters = new DataView();

        private bool RefreshCustomers()
        {
            try
            {
                matinDataSet.customerDataTable ocustomerDataTable
                        = new matinDataSet.customerDataTable();

                matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                    new matinDataSetTableAdapters.customerTableAdapter();

                ocustomerTableAdapter.FillCustomerByCustomers(ocustomerDataTable);

                if (ocustomerDataTable.Count != 0)
                {
                    matinDataSet.customerRow ocustomerRow = null;

                    for (int indexCount = 0; indexCount < ocustomerDataTable.Count; indexCount++)
                    {
                        ocustomerRow = null;
                        ocustomerRow = ocustomerDataTable[indexCount];

                        DataGridViewRow newCustomer = new DataGridViewRow();
                        newCustomer.CreateCells(dgvCustomers);

                        int index = -1;

                        newCustomer.Cells[++index].Value = ++Properties.Settings.Default.OrderRow;
                        newCustomer.Cells[++index].Value = ocustomerRow.cid;
                        newCustomer.Cells[++index].Value = ocustomerRow.ccodemelli;
                        newCustomer.Cells[++index].Value = ocustomerRow.cname;
                        newCustomer.Cells[++index].Value = ocustomerRow.cmobile;
                        newCustomer.Cells[++index].Value = ocustomerRow.cphone;
                        newCustomer.Cells[++index].Value = ocustomerRow.caddress;

                        dgvCustomers.Rows.Add(newCustomer);
                    }
                }
            }
            catch { }

            return true;
        }

        private void dgvCustomers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var senderGrid = (DataGridView)sender;

                if (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn &&
                    e.RowIndex >= 0)
                {
                    if (Convert.ToBoolean(dgvCustomers.Rows[e.RowIndex].Cells[0].Value) == false || dgvCustomers.Rows[e.RowIndex].Cells[0].Value == null)
                    {
                        dgvCustomers.Rows[e.RowIndex].Cells[0].Value = CheckState.Checked;
                    }
                    else
                    {
                        dgvCustomers.Rows[e.RowIndex].Cells[0].Value = CheckState.Unchecked;
                    }
                    dgvCustomers.EndEdit();
                }
            }
            catch { }
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgvCustomers.Rows)
                {
                    row.Cells[0].Value = CheckState.Checked;
                }
            }
            catch { }
        }

        private void btnDeselectAll_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgvCustomers.Rows)
                {
                    row.Cells[0].Value = CheckState.Unchecked;
                }
            }
            catch { }
        }

        private string DateFilter(string strFieldDateTable)
        {
            string strDateFilter = "";

            strDateFilter =
                strFieldDateTable + " BETWEEN '" + dateTimePickerOrderTime_Start.Value.ToString("yyyy/MM/dd") +
                                    "' AND '" + dateTimePickerOrderTime_End.Value.ToString("yyyy/MM/dd") + "'";

            return strDateFilter;
        }

        private string CustomerIDFilter()
        {
            string strCustomerIDFilter = "customer.cid=-1";

            try
            {
                foreach (DataGridViewRow row in dgvCustomers.Rows)
                {
                    if (Convert.ToBoolean(row.Cells[0].Value))
                    {
                        strCustomerIDFilter += " OR customer.cid=" + row.Cells[1].Value.ToString() + "";
                    }
                }
            }
            catch { }

            return strCustomerIDFilter;
        }

        private string ReportItemsRowFilter(string strFieldDateTable)
        {
            string strItemsRowFilter = "(1=1)";

            strItemsRowFilter += " AND (" + DateFilter(strFieldDateTable) + ")";

            strItemsRowFilter += " AND (" + CustomerIDFilter() + ")";

            return strItemsRowFilter;
        }

        private string ReportFactorsView()
        {
            string strFactorsView = "";

            strFactorsView =
                "SELECT factor.fid, factor.fdate, factor.fquantity," +
                      " customer.cid, customer.ccodemelli, customer.cname," +
                      " [order].oid," +
                      " product.pid, product.pcode, product.pname, product.pprice," +
                      " [order].ocount" +
                " FROM factor" +
                     " INNER JOIN customer ON factor.cid = customer.cid" +
                     " INNER JOIN [order] ON factor.fid = [order].fid" +
                     " INNER JOIN product ON [order].pid = product.pid";

            return strFactorsView;
        }

        private bool RefreshReportFactorView()
        {
            bool noError = true;

            string Q_ReportFactorLoad =
                "" + ReportFactorsView() +
                " WHERE " + ReportItemsRowFilter("factor.fdate") +
                " ORDER BY factor.fdate;";

            SqlCommand ReportFactorcmd = new SqlCommand(Q_ReportFactorLoad, my_cn);

            try
            {
                SqlDataAdapter ObjectAdapter = new SqlDataAdapter(ReportFactorcmd);
                DataSet ds = new DataSet();
                ObjectAdapter.Fill(ds, "factor");
                dvReportFilters.Table = ds.Tables["factor"];
            }
            catch
            {
                noError = false;
            }

            return noError;
        }

        private void btnFactors_Click(object sender, EventArgs e)
        {
            try
            {
                if (RefreshReportFactorView())
                {
                    dgvReports.DataSource = null;
                    dgvReports.Columns.Clear();

                    dgvReports.DataSource = dvReportFilters;

                    int index = -1;

                    dgvReports.Columns[++index].HeaderText = "شناسه فاکتور";
                    dgvReports.Columns[++index].HeaderText = "تاریخ ثبت";
                    dgvReports.Columns[++index].HeaderText = "مبلغ فاکتور";
                    dgvReports.Columns[++index].HeaderText = "شناسه مشتری";
                    dgvReports.Columns[++index].HeaderText = "کد ملی مشتری";
                    dgvReports.Columns[++index].HeaderText = "نام مشتری";
                    dgvReports.Columns[++index].HeaderText = "شناسه سفارش";
                    dgvReports.Columns[++index].HeaderText = "شناسه کالا";
                    dgvReports.Columns[++index].HeaderText = "کد کالا";
                    dgvReports.Columns[++index].HeaderText = "نام کالا";
                    dgvReports.Columns[++index].HeaderText = "قیمت کالا";
                    dgvReports.Columns[++index].HeaderText = "تعداد کالا";
                }
            }
            catch { }
        }

        private string ReportCriditsView()
        {
            string strCriditsView = "";

            strCriditsView =
                "SELECT cridit.crid, cridit.crdate, cridit.crprice," +
                      " customer.cid, customer.ccodemelli, customer.cname" +
                " FROM cridit" +
                     " INNER JOIN customer ON cridit.cid = customer.cid";

            return strCriditsView;
        }

        private bool RefreshReportCriditView()
        {
            bool noError = true;

            string Q_ReportCriditLoad =
                "" + ReportCriditsView() +
                " WHERE " + ReportItemsRowFilter("cridit.crdate") +
                " ORDER BY cridit.crdate;";

            SqlCommand ReportCriditcmd = new SqlCommand(Q_ReportCriditLoad, my_cn);

            try
            {
                SqlDataAdapter ObjectAdapter = new SqlDataAdapter(ReportCriditcmd);
                DataSet ds = new DataSet();
                ObjectAdapter.Fill(ds, "cridit");
                dvReportFilters.Table = ds.Tables["cridit"];
            }
            catch
            {
                noError = false;
            }

            return noError;
        }

        private void btnIncreaseCredit_Click(object sender, EventArgs e)
        {
            try
            {
                if (RefreshReportCriditView())
                {
                    dgvReports.DataSource = null;
                    dgvReports.Columns.Clear();

                    dgvReports.DataSource = dvReportFilters;

                    int index = -1;

                    dgvReports.Columns[++index].HeaderText = "شناسه اعتبار";
                    dgvReports.Columns[++index].HeaderText = "تاریخ ثبت";
                    dgvReports.Columns[++index].HeaderText = "مبلغ اعتبار";
                    dgvReports.Columns[++index].HeaderText = "شناسه مشتری";
                    dgvReports.Columns[++index].HeaderText = "کد ملی مشتری";
                    dgvReports.Columns[++index].HeaderText = "نام مشتری";
                }
            }
            catch { }
        }

        private void btnAccountBalance_Click(object sender, EventArgs e)
        {
            try
            {
                if (true)
                {
                    dgvReports.DataSource = null;
                    dgvReports.Columns.Clear();

                    dgvReports.Columns.Add("cId_Ab", "شناسه مشتری");
                    dgvReports.Columns.Add("cCodeMelli_Ab", "کد ملی مشتری");
                    dgvReports.Columns.Add("cName_Ab", "نام مشتری");
                    dgvReports.Columns.Add("AccountBalance_Ab", "موجودی حساب");
                    dgvReports.Columns.Add("cMobile_Ab", "موبایل");
                    dgvReports.Columns.Add("cPhone_Ab", "تلفن");
                    dgvReports.Columns.Add("cAddress_Ab", "آدرس");

                    matinDataSet.customerDataTable ocustomerDataTable
                        = new matinDataSet.customerDataTable();

                    matinDataSetTableAdapters.customerTableAdapter ocustomerTableAdapter =
                        new matinDataSetTableAdapters.customerTableAdapter();

                    matinDataSetTableAdapters.factorTableAdapter ofactorTableAdapter =
                        new matinDataSetTableAdapters.factorTableAdapter();

                    matinDataSetTableAdapters.criditTableAdapter ocriditTableAdapter =
                        new matinDataSetTableAdapters.criditTableAdapter();

                    matinDataSet.customerRow ocustomerRow = null;

                    foreach (DataGridViewRow row in dgvCustomers.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value))
                        {
                            ocustomerTableAdapter.FillCustomerByID(ocustomerDataTable, Int32.Parse(row.Cells[1].Value.ToString()));

                            if (ocustomerDataTable.Count != 0)
                            {
                                DataGridViewRow newCustomer = new DataGridViewRow();
                                newCustomer.CreateCells(dgvReports);

                                ocustomerRow = null;
                                ocustomerRow = ocustomerDataTable[0];

                                int indexC = -1;

                                newCustomer.Cells[++indexC].Value = ocustomerRow.cid;
                                newCustomer.Cells[++indexC].Value = ocustomerRow.ccodemelli;
                                newCustomer.Cells[++indexC].Value = ocustomerRow.cname;

                                long? cridit_SumPrice = ocriditTableAdapter.SumPriceByCustomerID(ocustomerRow.cid);
                                long? factor_SumQuantity = ofactorTableAdapter.SumQuantityByCustomerID(ocustomerRow.cid);

                                if (cridit_SumPrice == null)
                                {
                                    cridit_SumPrice = 0;
                                }

                                if (factor_SumQuantity == null)
                                {
                                    factor_SumQuantity = 0;
                                }

                                newCustomer.Cells[++indexC].Value =
                                    (cridit_SumPrice - factor_SumQuantity);

                                newCustomer.Cells[++indexC].Value = ocustomerRow.cmobile;
                                newCustomer.Cells[++indexC].Value = ocustomerRow.cphone;
                                newCustomer.Cells[++indexC].Value = ocustomerRow.caddress;

                                dgvReports.Rows.Add(newCustomer);
                            }
                        }
                    }
                }
            }
            catch { }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshReportForm();
        }

        private bool RefreshReportForm()
        {
            try
            {
                dateTimePickerOrderTime_Start.Value = DateTime.Now;
                dateTimePickerOrderTime_End.Value = DateTime.Now;
                dgvCustomers.Rows.Clear();
                try
                {
                    dgvReports.DataSource = null;
                    dgvReports.Rows.Clear();
                    dgvReports.Columns.Clear();
                }
                catch { }

                Properties.Settings.Default.OrderRow = 0;

                RefreshCustomers();
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            try
            {
                RefreshReportForm();
            }
            catch { }
        }

    }
}
